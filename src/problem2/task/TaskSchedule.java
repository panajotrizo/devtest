package problem2.task;

public class TaskSchedule {

    public static final String TASK_SCHEDULE_DAILY = "Daily";
    public static final String TASK_SCHEDULE_WEEKLY = "Weekly";
    public static final String TASK_SCHEDULE_MONTHLY = "Monthly";

    private String scheduleType;
    private String startTime;
    private boolean isOnRepeat;
    private String repeatTime;


    public String getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(String scheduleType) {
        this.scheduleType = scheduleType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getRepeatTime() {
        return repeatTime;
    }

    public void setRepeatTime(String repeatTime) {
        this.repeatTime = repeatTime;
    }

    @Override
    public String toString() {
        return "Schedule type = " + getScheduleType() + ", run at " + getStartTime() + (isOnRepeat() ? " and repeat every " + getRepeatTime() : "");
    }

    public boolean isOnRepeat() {
        return isOnRepeat;
    }

    public void setOnRepeat(boolean onRepeat) {
        isOnRepeat = onRepeat;
    }
}
