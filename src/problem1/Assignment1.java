package problem1;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class Assignment1 {

    public static void main(String[] args) {
        new Assignment1().start();
    }

    private void start() {
        String haystackString = "vnk2435kvabco8awkh125kjneytbcd12qjhb4acd123xmnbqwnw4t";
//        System.out.println("hayStackBytes = " + Arrays.toString(hayStackBytes));
        String needleString = "abcd1234";
//        System.out.println("needleBytes = " + Arrays.toString(needleBytes));
        int thresholdLimit = 3;
        findOccurrences(haystackString, needleString, thresholdLimit);
    }

    private void findOccurrences(String haystackString, String needleString, int thresholdLimit) {

        byte[] hayStack = getByteArray(haystackString);
        byte[] needle = getByteArray(needleString);

        int charsMatching = 0;
        int needleLength = needle.length;
        int haystackLength = hayStack.length;
        LinkedHashMap<String, Integer> occurrences = new LinkedHashMap();
        for (int i = 0; i < needleLength; i++) {

            for (int j = 0; j < haystackLength; j++) {
                if (needle[i] == hayStack[j]) {
                    charsMatching++;
                    for (int k = 1; k < needleLength; k++) {
                        if (i + k >= needleLength || j + k >= haystackLength) {
                            break;
                        }
                        byte b = needle[i + k];
                        if (b != hayStack[j + k]) {
                            charsMatching = 0;
                            break;
                        }
                        charsMatching++;
                        if (charsMatching >= thresholdLimit && (i == 0 || (needle[i - 1] != hayStack[j - 1] && i > 0))) {
                            occurrences.put(j + "/" + i, charsMatching);
                        }
                    }
                } else {
                    charsMatching = 0;
                }

                if (j > haystackLength - thresholdLimit) {
                    break;
                }
            }
        }

        for (Map.Entry<String, Integer> entry : occurrences.entrySet()) {
            String key = entry.getKey();
            System.out.println("Sequence of haystackLength = " + entry.getValue() + " found at haystack offset " + key.split("/")[0] + ", needle offset " + key.split("/")[1]);
        }
    }

    private byte[] getByteArray(String haystack) {
        String representation1 = Arrays.toString(haystack.getBytes());
        String[] byteValues = representation1.substring(1, representation1.length() - 1).split(",");
        byte[] bytes = new byte[byteValues.length];
        for (int i = 0, len = bytes.length; i < len; i++) {
            bytes[i] = Byte.parseByte(byteValues[i].trim());
        }
        return bytes;
    }


}
