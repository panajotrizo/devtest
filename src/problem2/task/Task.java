package problem2.task;

import org.w3c.dom.Element;

import java.util.ArrayList;

public class Task {

    private String taskName;
    private ArrayList<TaskSchedule> taskSchedules;

    public Task(Element node) throws Exception {
        setTaskName(node.getElementsByTagName("taskName").item(0).getTextContent());
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public ArrayList<TaskSchedule> getTaskSchedules() {
        if (taskSchedules == null) {
            taskSchedules = new ArrayList();
        }
        return taskSchedules;
    }

    public void setTaskSchedules(ArrayList<TaskSchedule> taskSchedules) {
        this.taskSchedules = taskSchedules;
    }

    public void addTaskSchedule(TaskSchedule taskSchedule) {
        getTaskSchedules().add(taskSchedule);
    }

    @Override
    public String toString() {
        return getTaskName() + " - " + getTaskSchedules();
    }
}
