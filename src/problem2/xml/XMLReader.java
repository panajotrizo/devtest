package problem2.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import problem2.task.Task;
import problem2.task.TaskSchedule;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;

public class XMLReader {

    private String fileName = "";

    public XMLReader(String pFileName) {
        this.fileName = pFileName;
    }

    public ArrayList<Task> readFormatsFromXML() {
        try {
            return readFormats(getFileName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    private ArrayList<Task> readFormats(String filename) throws Exception {
        Document doc = parseXmlFile(filename);
        assert doc != null;
        NodeList elementsByTagName = doc.getElementsByTagName("task");

        ArrayList<Task> allTasks = new ArrayList<>();

        for (int i = 0; i < elementsByTagName.getLength(); i++) {
            Element taskElement = (Element) elementsByTagName.item(i);
            Task task = new Task(taskElement);
            NodeList taskSchedules = taskElement.getElementsByTagName("taskSchedule");
            for (int j = 0; j < taskSchedules.getLength(); j++) {
                Element taskScheduleElement = (Element) taskSchedules.item(j);
                TaskSchedule taskSchedule = new TaskSchedule();
                taskSchedule.setScheduleType(taskScheduleElement.getElementsByTagName("type").item(0).getTextContent());
                taskSchedule.setStartTime(taskScheduleElement.getElementsByTagName("startTime").item(0).getTextContent());
                NodeList repeatTime = taskScheduleElement.getElementsByTagName("repeatTime");
                boolean hasRepetition = repeatTime != null && repeatTime.getLength() > 0;
                taskSchedule.setOnRepeat(hasRepetition);
                taskSchedule.setRepeatTime(hasRepetition ? repeatTime.item(0).getTextContent() : null);

                task.addTaskSchedule(taskSchedule);
            }
            allTasks.add(task);
        }
        return allTasks;
    }

    private static Document parseXmlFile(String configFile) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);
            return factory.newDocumentBuilder().parse(new File(configFile));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
