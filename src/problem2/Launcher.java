package problem2;

import problem2.task.Task;
import problem2.xml.XMLReader;

import java.util.ArrayList;

public class Launcher {


    public static void main(String[] args) {
        String configFileName = getCommandLineParameter(args, "filename");
        if (configFileName == null) {
            configFileName = "config.xml";
        }

        System.out.println("configFileName = " + configFileName);


        ArrayList<Task> tasks = new XMLReader(configFileName).readFormatsFromXML();
        System.out.println("tasks = " + tasks);

        new TaskScheduler(tasks).start();

    }

    public static String getCommandLineParameter(String[] args, final String fieldName) {
        if (args == null) {
            return null;
        }
        for (int i = 0; i < args.length; i++) {
            String keyToken = args[i];
            int valueTokenIndex = i + 1;
            if (keyToken.equalsIgnoreCase(fieldName) && valueTokenIndex < args.length) {
                return args[valueTokenIndex];
            }
        }
        return null;
    }

}
