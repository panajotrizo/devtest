package problem2;

import problem2.task.Task;
import problem2.task.TaskSchedule;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TaskScheduler {

    private ArrayList<Task> tasks;


    public TaskScheduler(ArrayList<Task> pTasks) {
        this.tasks = pTasks;
    }

    public void start() {
        ArrayList<Task> tasks = getTasks();
        if (tasks == null || tasks.isEmpty()) {
            System.out.println("No tasks scheduled");
            return;
        }

        ScheduledExecutorService execService = Executors.newScheduledThreadPool(5);

        for (Task task : tasks) {
            ArrayList<TaskSchedule> taskSchedules = task.getTaskSchedules();
            for (TaskSchedule taskSchedule : taskSchedules) {
                addTaskSchedule(execService, task, taskSchedule);
            }
        }


    }

    private void addTaskSchedule(ScheduledExecutorService execService, Task task, TaskSchedule taskSchedule) {
        Runnable runnable = () -> System.out.println("Task name " + task.getTaskName() +
                " schedule " + taskSchedule.getScheduleType() +
                " executing at: " + new Date());


        int startTime = 0;
        int nextFireTime = Integer.parseInt(taskSchedule.getStartTime());
        if(taskSchedule.getScheduleType().equals(TaskSchedule.TASK_SCHEDULE_DAILY)){
            startTime = getDelayUntilNextFireTime(nextFireTime);
        }else if(taskSchedule.getScheduleType().equals(TaskSchedule.TASK_SCHEDULE_WEEKLY)
                || taskSchedule.getScheduleType().equals(TaskSchedule.TASK_SCHEDULE_MONTHLY)){
//            startTime = 0; // todo
        }

        System.out.println("startTime afer = " + startTime);
        System.out.println("taskSchedule.isOnRepeat() = " + taskSchedule.isOnRepeat());
        System.out.println("taskSchedule.getScheduleType() = " + taskSchedule.getScheduleType());

        if(taskSchedule.isOnRepeat()){
            int repetition = Integer.parseInt(taskSchedule.getRepeatTime());
            execService.scheduleAtFixedRate(runnable, startTime, repetition, TimeUnit.HOURS);
        }else{
            execService.schedule(runnable, startTime, TimeUnit.HOURS);
        }


    }

    private int getDelayUntilNextFireTime(int nextFireTime) {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        return hour < nextFireTime ? nextFireTime - hour : nextFireTime - hour + 24;
    }

    public ArrayList<Task> getTasks() {
        return tasks;
    }

    public void setTasks(ArrayList<Task> tasks) {
        this.tasks = tasks;
    }
}
